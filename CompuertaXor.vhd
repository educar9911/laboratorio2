library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CompuertaXor is
  port (
  a : in std_logic;
    b : in std_logic;
      y : out std_logic
   );
end CompuertaXor;

architecture Behavioral of CompuertaXor is

begin

y <= a xor b;

end Behavioral;
