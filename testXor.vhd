library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity testXor is
end testXor;

architecture testbench of testXor is
component xor1
port(
 a : in std_logic;
 b : in std_logic;
 y : out std_logic
);
end component;

signal a : std_logic;
signal b : std_logic;
signal y : std_logic;

begin
XORTB:xor1
port map(a=>a, b=>b, y=>y);
TBDATA: process
begin
a <= '0';
b <= '0';

wait for 10 ns;
a <= '0';
b <= '1';


wait for 10 ns;
a <= '1';
b <= '0';


wait for 10 ns;
a <= '1';
b <= '1';


wait for 10 ns;
wait;
end process TBDATA;

end testbench;
